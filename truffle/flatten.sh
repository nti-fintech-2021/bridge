if [ -d build/flats ]; then
  rm -rf build/flats
fi

mkdir -p build/flats

truffle-flattener contracts/Storage.sol > build/flats/Storage_flat.sol
