import Vue from 'vue'
import VueI18n from 'vue-i18n'
import axios from 'axios'
import getBrowserLocale from "./utils/get-browser-locale"
import supportedLocales from "./locales/supported-locales"

Vue.use(VueI18n)

function supportedLocalesInclude(locale) {
  return Object.keys(supportedLocales).includes(locale)
}

function getStartingLocale() {
  const browserLocale = getBrowserLocale({ countryCodeOnly: true })

  if (supportedLocalesInclude(browserLocale)) {
    return browserLocale
  } else {
    return process.env.VUE_APP_I18N_LOCALE || "en"
  }
}

const startingLocale = getStartingLocale()

export const i18n = new VueI18n({
  locale: startingLocale, // установка локализации
  fallbackLocale: 'en',
  messages: {} // установка сообщений локализации
})

const loadedLanguages = [] // список локализаций, которые пред-загружены

function setI18nLanguage(lang) {
  i18n.locale = lang
  axios.defaults.headers.common['Accept-Language'] = lang
  document.querySelector('html').setAttribute('lang', lang)
  return lang
}

export function loadLanguageAsync(lang) {
  // Если локализация та же
  if (loadedLanguages.length > 0 && i18n.locale === lang) {
    return Promise.resolve(setI18nLanguage(lang))
  }

  // Если локализация уже была загружена
  if (loadedLanguages.includes(lang)) {
    return Promise.resolve(setI18nLanguage(lang))
  }

  // Если локализация ещё не была загружена
  return import(
    /* webpackChunkName: "lang-[request]" */ `./locales/${lang}.json`
  ).then(messages => {
    i18n.setLocaleMessage(lang, messages.default)
    loadedLanguages.push(lang)
    return setI18nLanguage(lang)
  })
}

loadLanguageAsync(startingLocale)

export default i18n