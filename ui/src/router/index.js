import Vue from 'vue'
import VueRouter from 'vue-router'
import UI from '../views/UI.vue'
import systemroutes from './systemroutes'
import pageroutes from './pagesroutes'
import i18n, { loadLanguageAsync } from "@/i18n"

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    redirect: i18n.locale
  },
  {
    path: '/:lang/',
    name: 'UI',
    component: UI,
    children: pageroutes
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
].concat(systemroutes)

const router = new VueRouter({
  mode: 'history',
  routes
})

import supportedLocales from "../locales/supported-locales"

router.beforeEach((to, from, next) => {
  let lang = to.params.lang
  console.log(lang)
  if (!Object.keys(supportedLocales).includes(lang)) {
    lang = i18n.locale
    next(`/${lang}/404`)
  }
  else loadLanguageAsync(lang).then(() => next())
})

export default router
