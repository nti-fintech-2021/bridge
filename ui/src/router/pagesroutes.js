const Home = () => import('../views/Home')
const SecondPage = () => import('../views/SecondPage')
const pageroutes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/:lang/Dashboard',
        name: 'Second Page',
        component: SecondPage
    },
]
export default pageroutes