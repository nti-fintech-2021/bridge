const Error404 = () => import('../views/404')
const Systemroutes = [
    {
      path: '/:lang/404',
      name: 'Error 404',
      component: Error404
    },
    {
      path: "/:lang/*",
      redirect: '/:lang/404'
    },
]
export default Systemroutes