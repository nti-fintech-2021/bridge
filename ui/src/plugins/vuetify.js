import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

import { en, ru } from 'vuetify/es5/locale'

export default new Vuetify({
    lang: {
        locales: { en, ru },
        current: 'ru',
      },
      customVariables: ['./assets/variables.scss'],
      treeShake: true,
      theme: {
        dark : false,
        themes: {
          options: {
            customProperties: true
          },
          dark: {
            main: "#3F51B5",
            shades: '#3F51B5',
            wallet: "7986CB"
          },
          light: {
            main: "#3F51B5",
            shades: '#3F51B5',
            wallet: "7986CB"
          }
        }
      },
});
