import Storage from '../contracts/Storage.json'

const options = {
    web3: {
      block: false,
      fallback: {
        type: 'ws',
        url: 'ws://127.0.0.1:8545'
      }
    },
  
    // The contracts to monitor
    contracts: [Storage],
    polls: {
      blocks: 1500,
      // check accounts ever 15 seconds
      accounts: 15000
    },
    networkWhitelist: [1001]
  }
  
  export default options