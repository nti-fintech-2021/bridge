import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import i18n from './i18n'
import drizzleVuePlugin from '@drizzle/vue-plugin'

import drizzleOptions from './plugins/drizzleOptions'

Vue.config.productionTip = false

// Register the drizzleVuePlugin
Vue.use(drizzleVuePlugin, { store, drizzleOptions })

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App)
}).$mount('#app')
